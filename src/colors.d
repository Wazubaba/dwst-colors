module dwst.colors;

import std.string: isNumeric;
import std.conv: to;

const string NULLCOLOR = "\x1b[0m";
const string RESETCOLORS = "\x1b[39;49m";
const ubyte BLACK = 0;
const ubyte RED = 1;
const ubyte GREEN = 2;
const ubyte YELLOW = 3;
const ubyte BLUE = 4;
const ubyte MAGENTA = 5;
const ubyte CYAN = 6;
const ubyte WHITE = 7;

/++
	Converts an RGB value into the nearest web-safe color.
	Takes:
		ubyte r - red value
		ubyte g - green value
		ubyte b - blue value
	returns:
		ubyte rhs - valid web-safe color as a single value
++/
ubyte customColor(ubyte r, ubyte g, ubyte b)
{
	debug {import std.stdio: writeln; writeln("func: customColor: ",to!string((r*6/256)*36 + (g*6/256)*6 + (b*6/256)));}
	return to!ubyte((r*6/256)*36 + (g*6/256)*6 + (b*6/256));
}

/++
	Contains a valid colorsequence that can be fed to the colorize function.
++/
struct Color
{
	ubyte color;
	bool isBold;
	bool isDim;
	bool isItalic;
	bool isUnderlined;
	bool isBlinking;
	bool isFastBlinking;
	bool isReversed;
	bool isInvisible;
	bool isBackground;
	bool isCrossedout;

	string opCast(T: string)()
	{
		string rhs = "\x1b[";
		if (isBold) rhs ~= "1;";
		if (isDim) rhs ~= "2;";
		if (isItalic) rhs ~= "3;";
		if (isUnderlined) rhs ~= "4;";
		if (isBlinking) rhs ~= "5;";
		if (isFastBlinking) rhs ~= "6;";
		if (isReversed) rhs ~= "7;";
		if (isInvisible) rhs ~= "8;";
		if (isCrossedout) rhs ~= "9;";

		if (this.color == 0)
		{
			rhs = rhs[0..$-1]; // remove previous semicolon
			rhs ~= "m";
		}
		else
		{
			if (this.color < 8)
			{
				if (isBackground) rhs ~= "4";
				else rhs ~= "3";
			}
			else
			{
				if (isBackground) rhs ~= "48;5;";
				else rhs ~= "38;5;";
			}
			rhs ~= to!string(color) ~ "m";
		}

		debug { import std.stdio: writeln; writeln("escape code generated: ", rhs[1..$]);}

		return rhs;
	}
}

/++
	UFCS-style function to colorize a string with as many Color structs as one
	requires.

	Takes:
		string input - base string to colorize
		variadic args - Color structs to apply to the input string

	Returns:
		string rhs - string with all necessary escape codes added
++/
string colorize(string, S...)(string input, S args)
{
	string rhs;
	foreach (arg; args)
		rhs ~= cast(string) arg;

	rhs ~= input ~ NULLCOLOR ~ RESETCOLORS;
	return rhs;
}

unittest
{
	import std.stdio;

	Color c1 = {color: CYAN, isBold: true};
	Color c2 = {color: YELLOW, isBackground: true};
	Color c3 = {color: customColor(50,50,5)};
	Color c4 = {color: MAGENTA, isFastBlinking: true};
	Color c5 = {color: MAGENTA, isCrossedout: true};
	Color c6 = {isUnderlined:true};

	writeln("hai there!".colorize(c1, c2));

	writeln("test of rgb colors :D \\o/".colorize(c3));

	writeln("test of three colors at once template function :D".colorize(c1, c2, c3));

	writeln("test of blinking colors!".colorize(c4));

	writeln("test of cross out!".colorize(c5));

	writeln("test of combobreaker!".colorize(c1, c6));

}
