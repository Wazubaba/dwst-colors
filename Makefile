DC= dmd
DFLAGS=

all: release docs includes stripped

release:
	$(DC) $(DFLAGS) src/colors.d -release -O -lib -oflibcolors.a

debug:
	$(DC) $(DFLAGS) -g -debug -lib src/colors.d -oflibcolors-debug.a

unittest:
	$(DC) $(DFLAGS) -unittest -g -debug -main src/colors.d -ofunittest-libcolors

stripped: release
	strip libcolors.a

docs:
	$(DC) $(DFLAGS) -o- -D -Dfdocs/dwst/colors.html src/colors.d

includes:
	$(DC) $(DFLAGS) -o- -H -Hfincludes/dwst/colors.di src/colors.d

clean:
	-@$(RM) -r docs includes libcolors.a libcolors-debug.a unittest-libcolors unittest-libcolors.o
