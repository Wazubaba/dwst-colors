# l i b c o l o r s
A simple library for adding color to your stdout lines.

# C O M P I L I N G
Just run `make` for the default build.
The makefile supports the following primary rules:
* all - builds a stripped release of the lib, includes, and the documentation
* release - (default rule) builds the library in release mode with optimizations
* includes - builds *.di include files
* debug - builds a debugging build of the library, containing debugging
	symbols and with debug code enabled
* unittest - build the unittest
* docs - build inline documentation
* clean - deletes generated build files

